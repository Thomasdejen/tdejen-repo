#!/bin/bash

echo 'Date' >> /tmp/serverinfo.info
date >> /tmp/serverinfo.info

echo "Last 10 users who logged to a server" >> /tmp/serverinfo.info
last -n 10 >> /tmp/serverinfo.info

echo "Swap space" >> /tmp/serverinfo.info
free -h >> /tmp/serverinfo.info

echo "Kernel Version" >> /tmp/serverinfo.info
uname -r >> /tmp/serverinfo.info

echo "IP address" >> /tmp/serverinfo.info
hostname -i >> /tmp/serverinfo.info


